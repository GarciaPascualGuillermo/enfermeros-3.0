<?php
return array(
    // set your paypal credential
   'client_id' => 'AWPAmtqMgmlkK-LvTkqsxVgXZGbQpzE7rYsAAgRX0Oa8DWpQo69wbkAdAC-R8p5twI-uCnjqKoldjdkI',
'secret'=> 'EAB9nwaa9pCh_iy0SGzn038tNT2gTaw54M7woK24oR7WekLH9w7YOolFB4NUe-e2hSJJzXtV7gbih3FE',

    /**
     * SDK configuration 
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);