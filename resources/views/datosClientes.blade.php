<!DOCTYPE html>
<html lang="en">
<head>
  <title>Ingrese sus Datos</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- icono para la pagina-->
<link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style type="text/css">

h2{
  color: black;
}
label{
  color: black;
}
</style>

<body>

<div class="container">
  <h2>Ingresa tus Datos </h2>
  <form class="form-horizontal" method="post"  action="/registroCliente" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label class="control-label col-sm-2">Nombre:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" placeholder="Ingrese nombre..." name="nombre" value="{{ Auth::user()->name }}" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Paterno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="25"placeholder="Ingrese apellido paterno..." name="paterno" requerid pattern="[A-Za-z ñ]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Materno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="25"placeholder="Ingrese apellido materno..." name="materno" required pattern="[A-Za-z ñ]+">
      </div>
    </div>

            <div class="form-group">

<label class="col-lg-3 control-label">Seleccione su sexo</label>
<div class="col-lg-9">
    <div class="radio">
        <label>
<input type="radio" name="sexxo" value="M" checked/> Mujer  </label>
</div>
<div class="radio">
<label>
<input type="radio" name="sexxo" value="H" /> Hombre
</label>

</div>
</div>
</div>
    

    <div class="form-group">
      <label class="control-label col-sm-2">e-mail:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" maxlength="30"placeholder="Ingrese su correo..." name="correo"value="{{ Auth::user()->correo }}" required>

      </div>
    </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Telefono:</label>
        <div class="col-sm-10">
          <input type="number" class="form-control" maxlength="10" placeholder="Ingresa nùmero Telefonico..." name="telefono" numeross="[0-9]{10}" >
        </div>
      </div>


    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" href="/panelCliente">Guardar</button>
    </div>
  </form>
</div>

</body>
</html>
