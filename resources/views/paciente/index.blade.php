<table class="thead-light">

  <thead class="thead-ligth">
    <tr>
      <th>#</th>
      <th>Nombre</th>
      <th>Apellido Paterno</th>
      <th>Apellido Materno</th>
      <th>NSS</th>
      <th>Sexo</th>
      <th>Edad</th>
      <th>Peso</th>
      <th>Estatura</th>
      <th>Direccion</th>
      <th>Padecimiento</th>
      <th>Alergias</th>
      <th>Tipo Sanguineo</th>
      <th>Tratamientos</th>
      <th>Otros</th>
      <th>Acciones</th>

    </tr>
  </thead>

  <tbody>
    @foreach($paciente as $pacientes)
    <tr>
      <td>{{loop->iteration}}<td>
        <td>{{$paciente->Nombre}}</td>
        <td>{{$paciente->ApellidoPaterno}}</td>
        <td>{{$paciente->ApellidoMaterno}}</td>
        <td>{{$paciente->nss}}</td>
        <td>{{$paciente->edad}}</td>
        <td>{{$paciente->peso}}</td>
        <td>{{$paciente->estatura}}</td>
        <td>{{$paciente->direccion}}</td>
        <td>{{$paciente->padecimientos}}</td>
        <td>{{$paciente->alergias}}</td>
        <td>{{$paciente->tiposangre}}</td>
        <td>{{$paciente->tratamientos}}</td>
        <td>{{$paciente->otros}}</td>
        <td>
          <a href="{{url('/paciente/'.$pacientes->id.'/edit')}}">
            editar
          </a>
            <form method="post" action="{{url/paciente/'.$pacientes->id'}}">
              {{csrf_field}}
              {{method_field('DELETE')}}
              <button type=#submit onclick="return confirm('¿Desea Eliminar?');">Eliminar</button>
            </form>
        </td>
    </tr>
    @endforeach
  </tbody>
</table>
