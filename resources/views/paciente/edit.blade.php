<form action="{{url('/paciente/'.$pacientes->id)}}" method="post" enctype="multipart/form-data">
{{csrf_field()}}
{{method_field('PATCH')}}
  <div class="form-group">
    <label class="control-label col-sm-2">Nombre del Paciente:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="10" placeholder="Ingrese nombre(s)" name="username" value="{{$pacientes->Nombre}}" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Apellido Paterno:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="10" placeholder="Ingrese apellido paterno" name="username" value="{{$pacientes->ApellidoPaterno}}" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Apellido Materno:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="10" placeholder="Ingrese apellido materno" name="username" value="{{$pacientes->ApellidoMaterno}}"  required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">NSS:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="30"placeholder="Ingrese el nss" value="{{$pacientes->nss}}" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Sexo:</label>
  <div class="col-sm-10">
    <select name="tiposexo" value="{{$pacientes->sexo}}">
      <option selected>Elija sexo</option>
      <option>Femenino</option>
      <option>Masculino</option>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Edad del paciente:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="18"placeholder="Ingrese la edad" value="{{$pacientes->edad}}">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Peso:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="30" placeholder="Ingrese el peso en kg" value="{{$pacientes->peso}}" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Estatura:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="30"placeholder="Ingrese la estatura" value="{{$pacientes->estatura}}" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Direccion:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="30"placeholder="Ingrese la direccion" value="{{$pacientes->direccion}}" required>
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-sm-2">Padecimiento:</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" maxlength="30"placeholder="Ingrese el nombre del padecimiento" value="{{$pacientes->padecimiento}}" required>
    </div>
  </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Alergias:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese las alergias" value="{{$pacientes->alergias}}" required>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2">Tipo de Sangre:</label>
    <div class="col-sm-10">
      <select name = "Tipo de sangre" placeholder="Ingrese los tratamientos que lleva el paciente" value="{{$pacientes->tiposangre}}" required>
        <option selected>Elija tipo sanguineo</option>
        <option>O Positivo</option>
        <option>O Negativo</option>
        <option>A Positivo</option>
        <option>A Negativo</option>
        <option>B Positivo</option>
        <option>B Negativo</option>
        <option>AB Positivo</option>
        <option>AB Negativo</option>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2">Tratamientos:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingreselos tratamientos que lleva el paciente" value="{{$pacientes->tratamientos}}" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Otros:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="200"placeholder="Informacion adicional" value="{{$pacientes->otros}}" required>
      </div>
    </div>

    <input type="submit" value="Editar">
</form>
