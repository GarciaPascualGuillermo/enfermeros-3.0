<!DOCTYPE html>
<html>
<head>

  <title>Panel de Cliente</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <!-- icono para la pagina-->


  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <!-- icono para la pagina-->
<link rel="shortcut icon" href="{{ asset('imagenes/enfermera.png" type="image/png')}}">
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{ asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700')}}" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/panelCliente" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contacto</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->

  </nav>
  <!-- /.navbar -->
 <?php 
        
         $costo=0;
         $username = "root";
          $password = "";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
          $servername = "localhost";
          $database = "enfermeres";
          $conexion = mysqli_connect($servername, $username, $password);
          $db = mysqli_select_db( $conexion, $database );
          $consulta = "select enfermero.costo from enfermero  inner join servicio  on servicio.id_enfermero = enfermero.id_enfermero where enfermero.id_enfermero='$id'";
          $result=mysqli_query($conexion,$consulta);
          while($row = mysqli_fetch_array($result)){
         $costo=$row[0];
        
        }

       ?>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/panelCliente" class="brand-link">

      <span class="brand-text font-weight-light">Panel de Cliente</span>
    </a>


    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">

        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ¿Que desea hacer?
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
  
              <li class="nav-item">
                <a href="/catalogo" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Catalogo de Enfermeros</p>
                </a>
              </li>

                
                     <li class="nav-item">
                <a href="/pendienteActivacion" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pendientes de Pago</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/pendientePago" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>En curso</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/Historial" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Historial de Servicios</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              </li>

            </ul>
          </li>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>


<div >
  <div class="">
    <h2>Ingrese los datos para continuar con su solicitud</h2>
  </div>

  <form action="/registrarPaciente" method="post" enctype="multipart/form-data">
  {{csrf_field()}}
    <div class="form-group">
      <label class="">Nombre:</label>
      <div class="">
        <input type="text" class="form-control" maxlength="100" placeholder="Ingrese nombre completo" name="nombre" required pattern="[A-Za-z ÑñáéíóúÁÉÍÓÚ]+">
      </div>
    </div>
    <div class="form-group">
    <label class="">Seleccione su sexo:</label>
<div class="col-lg-9">
    <div class="radio">
        <label ><input type="radio" name="sexo" value="M" checked/>Mujer</label>
    </div>
      <div class="radio">
    <label><input type="radio" name="sexo" value="H" />Hombre</label>
      </div>
    </div>
  </div>
    <div class="form-group">
      <label class="">Edad:</label>
        <div class="">
          <input type="number" min="1" max="60" class="form-control" maxlength="2" placeholder="Ingrese su edad" name="edad"required pattern="[0-9]{2}+" >
        </div>
    </div>
    <div class="form-group">
      <label class=" ">Peso:</label>
      <div class="">
        <input type="number"  step="0.01" min="0.5" max="100" class="form-control" maxlength="30" placeholder="Ingrese el peso en Kilogramos" name="peso" required>
      </div>
    </div>
    <div class="form-group">
      <label class=" ">Estatura:</label>
      <div class="">
        <input type="number" min="30" max="200" class="form-control" maxlength="3"placeholder="Ingrese la estatura en centimetros" name="estatura" required  patter="[0-9]{3}">
      </div>
    </div>
    <div class="form-group">
      <label class=" ">Direccion:</label>
      <div class="">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese la direccion" name="direccion" required  patter="[A-Za-zÑ ñáéíóúÁÉÍÓÚ,.:;#/]+">
      </div>
    </div>
      <div class="form-group">
        <label class=" ">Alergias:</label>
        <div class="">
          <input type="text" class="form-control" maxlength="30"placeholder="Ingrese las alergias" name="alergia" required patter="[A-Za-zÑ ñáéíóúÁÉÍÓÚ,;]+">
        </div>
      </div>
       <div class="form-group">
          <label class=" ">Tipo sanguineo: </label>
          <select name="tipo">
            <option value="A+" selected="selected">A+</option>
            <option value="A-">A-</option>
            <option value="B+">B+</option>
            <option value="B-">B-</option>
            <option value="O+">O+</option>
            <option value="O-">O-</option>
            <option value="AB-" >AB- </option>
            <option value="AB+">AB+</option></select>
        </div>
      <div class="form-group">
        <label class=" ">Descripcion de cuidados:</label>
        <div class="">
          <textarea name="message" rows="10" cols="50" required patter="[A-Za-zÑ ñáéíóúÁÉÍÓÚ,.:;]+"></textarea>
        </div>
      </div>

      <div class="form-group">
        <label class=" ">Fecha Inicial</label>
        <div class="col-sm-10">
          <?php date_default_timezone_set('America/Mexico_City');?>
          <input onkeyup="horas()" type='datetime-local' value ="<?php echo date('Y-m-d').'T'.date('h:i');?>" name='fechaInicio' id="fechaInicio" min="<?php echo date('Y-m-d').'T'.date('h:i'); ?>"/>
        </div>
      </div>
      <div class="form-group">
        <label class=" ">Fecha de terminación</label>
        <div class="col-sm-10">
           
       <?php    
       $date=new dateTime();
       $date->modify('+2 hours');
       echo $date->format('H:i:s');
       ?>


            <input onkeyup="horas()" type='datetime-local' value ="<?php echo date('Y-m-d').'T'.date('h:i'); ?>" name='fechaFin' id="fechaFin" min="<?php echo date('Y-m-d').'T'.date('h:i'); ?>" />
        </div>
      </div>
      <div class="form-group">
        <label class=" ">Costo de Operacion:</label>s
        <div class="">
       <input type="text" name="costo" id="costo"  readonly="readonly" >
       
        </div>
      </div>
      <div class="col-sm-10">
        <input type="hidden" class="form-control"  name="id_cliente" value="{{ Auth::user()->id }}">
      </div>
      <div class="col-sm-10">
        <input type="hidden" class="form-control"  name="id_enfermero" value="<?php echo $id ?>">
      </div>
     

 
    <div class="col-sm-offset-2 ">
      <button  name="boton" id="boton" type="submit" class="btn btn-default">Guardar</button>
    </div>
  </form>

   {{--all my scripts goes here--}}
</div>

          </div><!-- /.col -->

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  <aside class="control-sidebar control-sidebar-dark">
  </aside>
</div>
<script type="text/javascript">
   function horas(){
     var costo = '<?php echo $costo;?>';
    var oldDate = document.getElementById("fechaInicio").value;
    var newDate = document.getElementById("fechaFin").value;
   fromDate = parseInt(new Date(oldDate).getTime()/1000); 
    toDate = parseInt(new Date(newDate).getTime()/1000);
    var timeDiff = (toDate - fromDate)/3600;  //will give difference in hrs
    var costoTotal = timeDiff*costo;
     document.getElementById('costo').value= costoTotal;
    }
</script>
</body>
<style media="screen">
textarea{
  width: 100%;
  border: 1px solid #000;
}
button {
  background: lightgrey;
  border: 0;
}
</style>
</html>
