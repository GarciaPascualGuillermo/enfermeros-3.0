<!DOCTYPE html>
 <html>
    <head>
         <title>this is my mater page</title>
         <style></style>
         <meta name="csrf-token" content="{{ csrf_token() }}">
          <meta name="viewport" content="width=device-width, initial-scale=1">
           <!-- icono para la pagina-->
<link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        
<div class="container">
  <h2>Ingresa tus Datos (Informacion Requerida para contratación)</h2>
  <form class="form-horizontal" method="post"  action="/registroEnfermero" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label class="control-label col-sm-2">Nombre:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30" placeholder="Ingrese nombre" name="nombre" required pattern="[A-Za-z]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Paterno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" placeholder="Ingrese apellido Paterno" name="paterno" required pattern="[A-Za-z]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Materno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" placeholder="Ingrese Apellido Materno" name="materno" required pattern="[A-Za-z]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Correo:</label>
      <div class="col-sm-10">
        <input  type="email" class="form-control" maxlength="30" placeholder="Ingrese su correo" name="correo" id="correo">
        <main>
                 <div id='result-username'></div>
             </main>
      </div>
      <div class="col-sm-2" name="result-username" id="result-username"></div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Contraseña:</label>
      <div class="col-sm-10">
        <input  type="password" class="form-control" maxlength="30"placeholder="Ingrese una contraseña" name="pass">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Dirección:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingresa Direccion actual" name="direccion" required>
      </div>
    </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Telefono:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="10" placeholder="Ingresa numero Telefonico"  name="telefono" required pattern="[0-9]{10}">

        </div>
          </div>
          <div class="form-group">

<label class="col-lg-3 control-label">Seleccione su sexo</label>
<div class="col-lg-9">
    <div class="radio">
        <label>
<input type="radio" name="sexo" value="M" checked/> Mujer  </label>
</div>
<div class="radio">
<label>
<input type="radio" name="sexo" value="H" /> Hombre
</label>

</div>
      <div class="form-group">
        <label class="control-label col-sm-2">Edad:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="2" placeholder="Ingrese su edad" name="edad"required pattern="[0-9]{2}" >
        </div>
    </div>

    </div>
        <div class="form-group">

<label class="col-lg-3 control-label">Seleccione su Escolaridad</label>
      <div class="col-lg-9">
          <div class="radio">
              <label>
  <input type="radio" name="escolaridad" value="Egresado" checked/> Egresado  </label>
      </div>
  <div class="radio">
      <label>
      <input type="radio" name="escolaridad" value="Pasante" /> Pasante
    </label>
  </div>
  <div class="radio">
      <label>
      <input type="radio" name="escolaridad" value="Otro" /> Otro
    </label>
  </div>
</div>
</div>

    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Cedula, Num. Credencial:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="18" placeholder="Ingrese sus datos" name="cedula" required pattern=[0-9]{8}>

      </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Achivo:</label>
        <div class="">
          <input type="file" class="" name="archivo" accept="application/pdf">

        </div>
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Actualizar</button>
    </div>

  </form>
       {{--all my scripts goes here--}}
       <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
       <script type = "text/javascript">
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $(document).ready(function() { 
          $('#correo').on('blur', function() {
        $('#result-username').html('<img src="images/loader.gif" />').fadeOut(1000);
 
        var correo = $(this).val();   
        var dataString = correo;
               
            $.ajax({
               url:'ajaxx',
               data:{'correo':dataString},
               type:'post',
               success:  function (response) {
                 $('#result-username').fadeIn(1000).html(response);
               },
               statusCode: {
                  404: function() {
                     alert('web not found');
                  }
               },
               error:function(x,xs,xt){
                  window.open(JSON.stringify(x));
                  //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
               }
            });
            });
        });
       </script>
    </body>
 </html>