<?php
$username = "root";
$password = "";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
$servername = "localhost";
$database = "enfermeres";
$conexion = mysqli_connect($servername, $username, $password);
$db = mysqli_select_db( $conexion, $database );

$result=mysqli_query($conexion,"select id_enfermero,foto, CedulaProf, Nombre,Apellido_pat,Apellido_mat,edad,escolaridad,Telefono, Direccion, correo,password ,status,contratacion,costo,descripcion FROM enfermero WHERE id_enfermero= '$id_enfermero'");

while($row = mysqli_fetch_array($result)){
   $id=$row['id_enfermero'];
    $foto=$row['foto'];
   $nombre=$row['Nombre'];
   $ap=$row['Apellido_pat'];
   $am=$row['Apellido_mat'];
   $edad=$row['edad'];
   $escolaridad=$row['escolaridad'];
   $telefono=$row['Telefono'];
   $direccion=$row['Direccion'];
   $costo=$row['costo'];
   $descripcion=$row['descripcion'];


}


?>


<!DOCTYPE html>
 <html>
    <head>
         <title>Home Care</title>
         <style></style>
         <meta name="csrf-token" content="{{ csrf_token() }}">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <!-- icono para la pagina-->
<link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    </head>
    <body>

<div class="container">
  <h2>Actualiza  tus Datos</h2>
  <form class="form-horizontal" method="post"  action="/UpdateEnfermero" enctype="multipart/form-data">
    @csrf


    

<div class="form-group">
      
       <div class="col-sm-10">
        <input type="hidden" class="form-control"   name="id_" readonly="readonly" value="<?php echo $id_enfermero;?>">
      </div>
    </div>


    <div class="form-group">
      <label class="control-label col-sm-2">Nombre:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" value="<?php echo $nombre ;?>" maxlength="30" placeholder="Ingrese nombre" name="nombre" required pattern="[A-Za-z ñÑ]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Paterno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10"  value="<?php echo $ap ;?>" placeholder="Ingrese apellido Paterno" name="paterno" required pattern="[A-Za-z ñÑ]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Materno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" value="<?php echo $am ;?>" placeholder="Ingrese Apellido Materno" name="materno" required pattern="[A-Za-z ñÑ]+">
      </div>
    </div>
    
   
    <div class="form-group">
      <label class="control-label col-sm-2">Dirección:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30" value="<?php echo $direccion ;?>" placeholder="Ingresa Direccion actual" name="direccion" required>
      </div>
    </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Telefono:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" value="<?php echo $telefono ;?>" maxlength="10" min="9" max="10" placeholder="Ingresa numero Telefonico"  name="telefono" required pattern="[0-9]{10}">

        </div>
          </div>
          <div class="form-group">

<label class="col-lg-3 control-label">Seleccione su sexo</label>
<div class="col-lg-9">
    <div class="radio">
        <label>
<input type="radio" name="sexo" value="M" checked/> Mujer  </label>
</div>
<div class="radio">
<label>
<input type="radio" name="sexo" value="H" /> Hombre
</label>

</div>
</div>
</div>
      <div class="form-group">
        <label class="control-label col-sm-2">Edad:</label>
        <div class="col-sm-10">
          <input type="text" min="18" max="60" class="form-control"  value="<?php echo $edad ;?>" maxlength="2" min="1"placeholder="Ingrese su edad" name="edad"required pattern="[0-9]{2}" >
        </div>
    </div>


        <div class="form-group">

  <label class="col-lg-3 control-label">Escolaridad: </label>

          <select name="escolaridad"> </option> <option value="Licenciatura"  selected="selected">Licenciatura</option>  <option value="Tecnico">Tecnico en enfermeria</option> <option value="Maestria">Maestria</option><option value="Doctorado">Doctorado</option></select>
</div>
    <div class="form-group">
      <label class="control-label col-sm-2">Costo: $</label>
      <div class="col-sm-10">
        <input type="number"class="form-control" minlength ="1" step="0.001" maxlength="6"  placeholder="Ingrese su cuota" name="costo" required pattern=[0-9.]>

      </div>
    </div>
   

    <div class="form-group">
      <label class="control-label col-sm-2">Descripcion:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="300" value="<?php echo $descripcion ;?>" placeholder="Ingresa una descripcion tuya" name="descripcion" required pattern="[A-Za-z ñÑ]+">
      </div>
    </div>


<div class="form-group">
      <label class="control-label col-sm-2">Imagen de Perfil: </label>
      <div class="col-sm-10">
        <input type="file"class="form-control" name="imagen" >

      </div>
    </div>
   

    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" id="boton" name="boton">Actualizar</button>
    </div>

  </form>

       {{--all my scripts goes here--}}
       <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
       <script type = "text/javascript">
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $(document).ready(function() {
          $('#correo').on('blur', function() {
        $('#result-username').html('<img src="images/loader.gif" />').fadeOut(1000);

        var correo = $(this).val();
        var dataString = correo;

            $.ajax({
               url:'ajaxx',
               data:{'correo':dataString},
               type:'post',
               success:  function (response) {
                 $('#result-username').fadeIn(1000).html(response);
               },
               statusCode: {
                  404: function() {
                     alert('web not found');
                  }
               },
               error:function(x,xs,xt){
                  window.open(JSON.stringify(x));
                  //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
               }
            });
            });
        });
       </script>

       {{--all my scripts goes here--}}
       <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
       <script type = "text/javascript">
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $(document).ready(function() {
          $('#cedula').on('blur', function() {
        $('#result-username2').html('<img src="images/loader.gif" />').fadeOut(1000);

        var cedula = $(this).val();
        var dataString = cedula;

            $.ajax({
               url:'ajaxxcedula',
               data:{'CedulaProf':dataString},
               type:'post',
               success:  function (response) {
                 $('#result-username2').fadeIn(1000).html(response);
               },
               statusCode: {
                  404: function() {
                     alert('web not found');
                  }
               },
               error:function(x,xs,xt){
                  window.open(JSON.stringify(x));
                  //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
               }
            });
            });
        });
       </script>
    </body>
 </html>
