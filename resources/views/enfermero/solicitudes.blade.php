<?php
session_start();
//echo $_SESSION['tipo'];
if ($_SESSION['estado']!='Autenticado') {
   header("location: /seguridad");
   exit();
}
if ($_SESSION['tipo']!='enfermero') {
 header("location: /");
exit();
}


 ?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panel de Enfermero</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
<!-- Minified JS library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/panelEnfe" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contacto</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/panelEnfe" class="brand-link">

      <span class="brand-text font-weight-light">Panel Enfermero</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">

        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['usuario'] ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ¿Que desea hacer?
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <li class="nav-item">
                <a href="/perfilEnfe" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Perfil</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="/solicitudes" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Nuevas Solicitudes</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/solicitudesPagadas" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Solicitudes Pagadas</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/solicitudesProceso" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Solicitudes En Proceso</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="/HistorialEnfe" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Historial de Servicios</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/ContactoEnfe" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Contacto</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/cerrar" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Cerrar Sesion</p>
                </a>
              </li>
            </ul>
          </li>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <?php
  $var = $_SESSION['id'];
$conexion=mysqli_connect("localhost","root","","enfermeres") or die ("problemas con la conexion");
$result=mysqli_query($conexion,"select id_servicio, nombre_paciente, edad, sexo, peso, estatura, tipo_sanguineo, alergias, descripcion, direccion,
fecha_inicio, fecha_final FROM servicio where servicio.status='solicitud' and id_enfermero=$var ") or die ("Problemas en el select: " .mysqli_error($conexion));
echo "
<div class=\"container\"  style=\"position:absolute;top:100px;left:250px\">
<div class=\"col-md-8\">
  <div class=\"col-md-4\">
  <table class=\"table table-condensed table-striped\"  >
      <thead class=\"thead\">
      <tr>
      <th scope=\"col=\"id\"\">id</th>
      <th scope=\"col=\"paciente\"\">Paciente</th>
      <th scope=\"col=\"edad\"\">Edad</th>
      <th scope=\"col=\"sexo\"\">Sexo</th>
      <th scope=\"col=\"peso\"\">Peso</th>
      <th scope=\"col=\"estatura\"\">Estatura</th>
      <th scope=\"col=\"tipo\"\">Tipo Sanguineo</th>
      <th scope=\"col=\"alergias\"\">Alergias</th>
      <th scope=\"col=\"desc\"\">Descripcion</th>
      <th scope=\"col=\"direccion\"\">Dirección</th>
      <th scope=\"col=\"fechaI\"\">Fecha de Inicio</th>
      <th scope=\"col=\"fechaF\"\">Fecha de Terminacion</th>
      <th scope=\"col=\"aceptar\"\">Aceptar</th>
      <th scope=\"col=\"rechazar\"\">Rechazar</th>
      </tr>
      </thead>
    ";

while($row = mysqli_fetch_array($result)){

  echo "


    <thead>
      <td >".$row[0]."</td>
      <td>".$row[1]."</td>
      <td>".$row[2]."</td>
      <td>".$row[3]."</td>
      <td>".$row[4]."</td>
      <td>".$row[5]."</td>
      <td>".$row[6]."</td>
      <td>".$row[7]."</td>
      <td>".$row[8]."</td>
      <td>".$row[9]."</td>
      <td>".$row[10]."</td>
      <td>".$row[11]."</td>
      <td><a href='/aceptar/$row[0]'>Aceptar</a>"."</td>
      <td><a href='/rechazar/$row[0]'>Rechazar</a>"."</td>";
 }
 echo"
</thead>
  </table> 
     </div>
     </div>";

 

echo"</div>";


?>


    <!-- /.content-header -->

  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->



<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
