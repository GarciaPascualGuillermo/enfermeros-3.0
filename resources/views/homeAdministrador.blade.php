
<?php 
session_start();
echo $_SESSION['tipo'];

if ($_SESSION['estado']!='Autenticado') {
   include("seguridad.php");
} 
if ($_SESSION['tipo']!='administrador') {
 header("location: /");
 
}
//session_destroy();
?>

@extends('admTem')
@section('content')
<div class="enfermeras">
  <?php foreach ($enfermeras as $enfermera): ?>
    <div class="enfermeras">
      <h3>{{$enfermera -> Nombre}} {{$enfermera->Apellido_pat}} {{$enfermera->Apellido_mat}}</h3>
    </div>
    <div class="enf-info">
      <p>Cedula Profecional:{{$enfermera->CedulaProf}}</p>
      <p>
        <a href="#">Aceptada</a>
        <a href="{{route('enfermeras_lista', $enfermera-> slug)}}">Leer Mas</a>
      </p>
    </div>
  <?php endforeach; ?>

</div>
@stop

