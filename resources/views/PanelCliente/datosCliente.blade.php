<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Panel Cliente</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <!-- icono para la pagina-->
<link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/panelCliente" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contacto</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->

  </nav>
  <!-- /.navbar -->


  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/panelCliente" class="brand-link">

      <span class="brand-text font-weight-light">Panel de Cliente</span>
    </a>


    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">

        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ¿Que desea hacer?
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
  
              <li class="nav-item">
                <a href="/catalogo" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Catalogo de Enfermeros</p>
                </a>
              </li>

           
                     <li class="nav-item">
                <a href="/pendienteActivacion" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pendientes de Pago</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/pendientePago" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>En curso</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/Historial" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Historial de Servicios</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              </li>

            </ul>
          </li>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>



  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>


<div class="container">
  <h2>Actualiza Datos </h2>
  <form class="form-horizontal" method="post"  action="/registroCliente" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label class="control-label col-sm-2">Nombre:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" placeholder="Ingrese nombre..." name="nombre" value="{{ Auth::user()->name }}" required  pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$" >
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Paterno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="25"placeholder="Ingrese apellido paterno..." name="paterno" value="{{ Auth::user()->apellido_pat }}" required  pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Materno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="25"placeholder="Ingrese apellido materno..." name="materno" value="{{ Auth::user()->apellido_mat}}" pattern="^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$">
      </div>
    </div>
    <div class="form-group">

<label class="col-lg-3 control-label">Sexo</label>
  <div class="col-lg-9">
      <div class="radio">
          <label>
<input type="radio" name="sexo" value="{{ Auth::user()->sexo }}"  checked/> Mujer  </label>
  </div>
<div class="radio">
  <label>
  <input type="radio" name="sexo" value="{{ Auth::user()->sexo }}"  /> Hombre
</label>

</div>
</div>
 <div class="form-group">
        <label class="control-label col-sm-2">Telefono:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="10" placeholder="Ingresa nùmero Telefonico..." name="telefono"  value="{{ Auth::user()->telefono }}"required pattern="[0-9]{10}" >

        </div>
      </div>
    <div class="form-group">
      <label class="control-label col-sm-2">email:</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" maxlength="30"placeholder="Ingrese su correo" name="correo"value="{{ Auth::user()->email }} "readonly>

      </div>
    </div>
     

    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default" >Actualizar</button>
    </div>
  </form>
</div>

          </div><!-- /.col -->

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->



<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
</body>
</html>
