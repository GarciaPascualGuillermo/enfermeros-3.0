<!DOCTYPE html>
<html>
<head>
  
  <title>Panel de Administracion</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <!-- icono para la pagina-->
<link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <!-- icono para la pagina-->
<link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/panelCliente" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contacto</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/panelCliente" class="brand-link">

      <span class="brand-text font-weight-light">Panel de Administracion</span>
    </a>


    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">

        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ¿Que desea hacer?
                <i class="right fas fa-angle-left"></i>
              </p>s
            </a>
            <li class="nav-item">
                <a href="/registroPaciente" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Registrar un Paciente</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/catalogo" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Servicios Disponibles</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="/datosCliente" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Completar Registro</p>
                </a>
              </li>
              <li class="nav-item">

                <a href="/verPacientes" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ver Pacientes</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              </li>

            </ul>
          </li>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>


<div class="container">
  <h2>Ingrese los datos del Paciente</h2>
  <form action="/registrarPaciente" method="post" enctype="multipart/form-data">
  {{csrf_field()}}
    <div class="form-group">
      <label class="control-label col-sm-2">Nombre:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30" placeholder="Ingrese nombre(s)" name="nombre" required pattern="[A-Za-z ñ]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Paterno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" placeholder="Ingrese apellido paterno" name="app" required pattern="[A-Za-z ñ]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Apellido Materno:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" placeholder="Ingrese apellido materno" name="apm" required pattern="[A-Za-z ñ]+">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">NSS:</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" minlength="10" maxlength="11" placeholder="Ingrese el nss" name="nss" id="nss"  required pattern="[0-9]">
        <main>
                 <div id='result-username'></div>
             </main>
      </div>
    </div>
    <div class="form-group">
    <label class="col-lg-3 control-label">Seleccione su sexo</label>
<div class="col-lg-9">
    <div class="radio">
        <label><input type="radio" name="sexo" value="M" checked/> Mujer  </label>
    </div>
      <div class="radio">
    <label><input type="radio" name="sexo" value="H" /> Hombre</label>
      </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Edad del paciente:</label>
      <div class="col-sm-10">
        <input type="number" class="form-control" maxlength="2" placeholder="Ingrese la edad" name="edad" required pattern="[0-9]" min="1" max="100">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Peso:</label>
      <div class="col-sm-10">
        <input type="number"  step="0.01" min="0.5" max="100" class="form-control" maxlength="30" placeholder="Ingrese el peso en Kilogramos" name="peso" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Estatura:</label>
      <div class="col-sm-10">
        <input type="number" min="30" max="200" class="form-control" maxlength="30"placeholder="Ingrese la estatura en centimetros" name="estatura" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Direccion:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese la direccion" name="direccion" required >
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Padecimiento:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese el nombre del padecimiento" name="padecimiento" required patter="[A-Za-z ñ]+">
      </div>
    </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Alergias:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="30"placeholder="Ingrese las alergias" name="alergia" required patter="[A-Za-z ñ]+">
        </div>
      </div>
      <div class="form-group">
    <label class="col-lg-3 control-label">Seleccione su tipo sanguineo</label>
<div class="col-lg-9">
    <div class="radio">
        <label><input type="radio" name="tipo" value="A+" checked/> A+  </label>
    </div>
      <div class="radio">
    <label><input type="radio" name="tipo" value="A-" /> A-</label>
      </div>
      <div class="radio">
    <label><input type="radio" name="tipo" value="B+" /> B+</label>
      </div>
      <div class="radio">
    <label><input type="radio" name="tipo" value="B-" /> B-</label>
      </div>
      <div class="radio">
    <label><input type="radio" name="tipo" value="O+" checked/> O+</label>
      </div>
      <div class="radio">
    <label><input type="radio" name="tipo" value="O-" /> O-</label>
      </div>

      <div class="radio">
    <label><input type="radio" name="tipo" value="AB+" /> AB+</label>
      </div>      <div class="radio">
    <label><input type="radio" name="tipo" value="AB-" /> AB-</label>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2">Tratamientos:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="30"placeholder="Ingreselos tratamientos que lleva el paciente" name="tratamiento" required pattern="[A-Za-z ñ]+">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Otros:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="200"placeholder="Informacion adicional " name="otro"required pattern="[A-Za-z ñ]+">
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Cliente:</label>
        <div class="col-sm-10">
          <input  style="display:none" type="text" class="form-control" maxlength="200"  name="id_cliente" value="{{Auth::user()->id}}" >
        </div>
      </div>

    <div class="col-sm-offset-2 col-sm-10">
      <button  name="boton" id="boton" type="submit" class="btn btn-default">Guardar</button>
    </div>
  </form>

   {{--all my scripts goes here--}}
       <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
       <script type = "text/javascript">
         $.ajaxSetup({
             headers: {
                 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
         });
         $(document).ready(function() { 
          $('#nss').on('blur', function() {
        $('#result-username').html('<img src="images/loader.gif" />').fadeOut(1000);
 
        var nss = $(this).val();   
        var dataString = nss;
               
            $.ajax({
               url:'ajaxnsS',
               data:{'nss':dataString},
               type:'post',
               success:  function (response) {
                 $('#result-username').fadeIn(1000).html(response);
               },
               statusCode: {
                  404: function() {
                     alert('web not found');
                  }
               },
               error:function(x,xs,xt){
                  window.open(JSON.stringify(x));
                  //alert('error: ' + JSON.stringify(x) +"\n error string: "+ xs + "\n error throwed: " + xt);
               }
            });
            });
        });
       </script>
</div>

          </div><!-- /.col -->

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
</body>
</html>
