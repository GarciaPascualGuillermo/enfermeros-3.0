<!--

<?php //$message=Session::get('message');
//$connect = mysqli_connect("localhost", "root", "", "duki");

//?>

-->
@include('alerts.request')

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Sin titulo</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <img src="img/logo-blanco2.png"   width="100" height="60">

      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Home</a></li>
      <!--    <li><a href="#about"></a></li>
          <li><a href="{{ url('inicia') }}">Inicia Sesion</a></li>
          <li><a href="#pricing">Productos</a></li>
          <li><a href="#team">Equipo</a></li>
          <li><a href="#gallery">Galeria</a></li>

          -->
          <!--
          <li class="menu-has-children"><a href="">Drop Down</a>
            <ul>
              <li><a href="#">Drop Down 1</a></li>
              <li class="menu-has-children"><a href="#">Drop Down 2</a>
                <ul>
                  <li><a href="#">Deep Drop Down 1</a></li>
                  <li><a href="#">Deep Drop Down 2</a></li>
                  <li><a href="#">Deep Drop Down 3</a></li>
                  <li><a href="#">Deep Drop Down 4</a></li>
                  <li><a href="#">Deep Drop Down 5</a></li>
                </ul>
              </li>
              <li><a href="#">Drop Down 3</a></li>
              <li><a href="#">Drop Down 4</a></li>
              <li><a href="#">Drop Down 5</a></li>
            </ul>
          </li>
          -->
          <li><a href="#contact">Acerca de los enferme@s..</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->

 

  <section id="intro">
<!--
    <div class="intro-text">
      <h2>Bienvenido a </h2>
      <p>Te invitamos a buscar el paquete que se ajuste a tus necesidades</p>
      <a href="#about" class="btn-get-started scrollto">Comenzar</a>
    </div>

    
-->


  </section>
  
  <!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================
    <section id="about" class="section-bg">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title">OBX</h3>
          <span class="section-divider"></span>


          <p class="section-description">
          El sitio web oficial de OBX ya disponible. <br>
           El mejor sistema de facturación en línea al alcance y comodidad de tu hogar u oficina a todas horas.
          </p>
        </div>

        <div class="row">
          <div class="col-lg-6 about-img wow fadeInLeft">
            <img src="img/gallery/gallery-3.jpg" alt="">
          </div>

          <div class="col-lg-6 content wow fadeInRight">
            <h2>UN SISTEMA DE FACTURACION TOTALMENTE FUNCIONAL Y DISPONIBLE</h2>
            <h3>Disfrute de cada uno de los puntos llamativos de una facturación en línea sofisticada:</h3>
            <p>
            </p>

            <ul>
              <li><i class="ion-android-checkmark-circle"></i>Un servicio que permanece activo las 24 horas los 365 días del año para cualquier necesidad de facturación.</li>
              <li><i class="ion-android-checkmark-circle"></i>Siéntase cómodo ante cualquier percance, con un solo clic podrá contar con nuestro sistema en cualquier momento.</li>
              <li><i class="ion-android-checkmark-circle"></i>No necesita activar en el momento su funcionamiento, usted decide cuando lo requiere. </li>
            </ul>

            <p>
            Está pensado para el trabajo rudo y como tal se encargará del trabajo duro, sin retrasos, sin complicaciones, sin tramites costosos y tediosos.            </p>
          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Product Featuress Section
    ============================-
    <section id="features">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 offset-lg-4">
            <div class="section-header wow fadeIn" data-wow-duration="1s">
              <h3 class="section-title">CARACTERISTICAS DE DUKI FACTURAS EN LINEA</h3>
              <span class="section-divider"></span>
            </div>
          </div>

          <div class="col-lg-4 col-md-5 features-img">
            <img src="img/product-features.png" alt="" class="wow fadeInLeft">
          </div>

          <div class="col-lg-8 col-md-7 ">

            <div class="row">

              <div class="col-lg-6 col-md-6 box wow fadeInRight">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                <h4 class="title"><a href="">Sencillo y fácil de usar</a></h4>
                <p class="description">el flujo de trabajo depende de una interfaz bien pensada; Duki está hecho para usarse desde la primera vez como todo un experto en facturación en linea.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.1s">
                <div class="icon"><i class="ion-ios-flask-outline"></i></div>
                <h4 class="title"><a href="">Facturaciones</a></h4>
                <p class="description">Selecciona el tipo de factura que mejor te convenga según tu giro.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.2s">
                <div class="icon"><i class="ion-social-buffer-outline"></i></div>
                <h4 class="title"><a href="">Complementos de pago</a></h4>
                <p class="description"> Puedes realizar los complementos una vez recibidos los pagos por tus facturas.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.3s">
                <div class="icon"><i class="ion-ios-analytics-outline"></i></div>
                <h4 class="title"><a href="">Facturas de gastos en masa</a></h4>
                <p class="description">No te preocupes por las facturas masivas, Duki se encargará de almacenar y organizar tus facturas.</p>
              </div>
            </div>

          </div>

        </div>

      </div>

    </section><!-- #features -->

    

    <!--==========================
      Product Advanced Featuress Section
    ============================-
    <section id="advanced-features">

      <div class="features-row section-bg">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <img class="advanced-feature-img-right wow fadeInRight" src="img/advanced-feature-1.jpg" alt="">
              <div class="wow fadeInLeft">
                <h2>ACCEDA DESDE CUALQUIER SITIO Ó DISPOSITIVO CON ACCESO A INTERNET</h2>
                <p>Duki se encuentra disponible en cualquier dispositivo, ya sea móvil, tableta o de escritorio; Duki se adapta a las necesidades de los clientes y así llegar a más empresas. Puede desde consultar el estado de sus facturas hasta ver en tiempo real sus informes de los mismos y realizar reportes detallados de las facturas realizadas hasta ahora.</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="features-row">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <img class="advanced-feature-img-left" src="img/advanced-feature-2.jpg" alt="">
              <div class="wow fadeInRight">
                <h2>LLEVE EL TRABAJO SOBRE EL PAPEL A LO DIGITAL; UN GRAN PASO DE LA TECNOLOGIA DEDICADO A LAS EMPRESAS</h2>
                <i class="ion-ios-paper-outline" class="wow fadeInRight" data-wow-duration="0.5s"></i>
                <p class="wow fadeInRight" data-wow-duration="0.5s">•	Siéntase tranquilo en cuanto a las regulaciones fiscales, Duki siempre está un paso delante de la rapidez.</p>
                <i class="ion-ios-color-filter-outline wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="0.5s"></i>
                <p class="wow fadeInRight" data-wow-delay="0.2s" data-wow-duration="0.5s">Siempre contarás con asesoramiento técnico ante cualquier duda que tengas.</p>
                <i class="ion-ios-barcode-outline wow fadeInRight" data-wow-delay="0.4" data-wow-duration="0.5s"></i>
                <p class="wow fadeInRight" data-wow-delay="0.4s" data-wow-duration="0.5s">Siempre dispondrá de facturas hechas en línea.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
<!--
      <div class="features-row section-bg">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <img class="advanced-feature-img-right wow fadeInRight" src="img/advanced-feature-3.jpg" alt="">
              <div class="wow fadeInLeft">
                <h2>Duis aute irure dolor in reprehenderit in voluptate velit esse</h2>
                <h3>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam</p>
                <i class="ion-ios-albums-outline"></i>
                <p>Quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section> #advanced-features -->



    <!--==========================
      Pricing Section
    ============================-->
    <section id="pricing" class="section-bg">
      <div class="container">

        <div class="section-header">
          <h3 class="section-title"></h3>
          <span class="section-divider"></span>
          <p class="section-description"></p>
        </div>
        <div class="row">
<?php
				//$query = "SELECT * FROM paquetes ORDER BY id ASC";
				//$result = mysqli_query($connect, $query);
				//if(mysqli_num_rows($result) > 0)
				//{
				//	while($row = mysqli_fetch_array($result))
				//	{
				?>


<!--
            <div class="col-lg-4 col-md-6">
            <div class="box wow fadeInLeft">
              <h3><?php //echo $row["descripcion"]; ?></h3>
              <h4><sup>$</sup><?php //echo $row["precio_neto"]; ?><span> por <?php //echo $row["cantidad_folio"]; ?>  timbres</span></h4>
              <ul>
                <li><i class="ion-android-checkmark-circle"></i> Soporte tecnico</li>
                <li><i class="ion-android-checkmark-circle"></i> Faciles de usar</li>
                <li><i class="ion-android-checkmark-circle"></i> Siempre disponibles</li>
                <li><i class="ion-android-checkmark-circle"></i> Sin fecha de caducidad</li>
              </ul>
              <a href="/inicia" class="get-started-btn">Comprar</a>
            </div>
          </div>

    -->    
      
          <?php
					//}
			//	}
			?>
      </div>
      </div>
    </section><!-- #pricing -->


    <!--==========================
      Frequently Asked Questions Section
    ============================-
    <section id="faq">
      <div class="container">

        <div class="section-header">
          <h3 class="section-title">Preguntas frecuentes</h3>
          <span class="section-divider"></span>
          <p class="section-description">Antes de comunicarse por correo le invitamos a leer nuestras preguntas frecuentes</p>
        </div>

        <ul id="faq-list" class="wow fadeInUp">
          <li>
            <a data-toggle="collapse" class="collapsed" href="#faq1">¿Cómo puedo disponer de un paquete adquirido?<i class="ion-android-remove"></i></a>
            <div id="faq1" class="collapse" data-parent="#faq-list">
              <p>
              Al momento de hacer la compra, se envian los folios que estaran disponibles las 24 horas del dia.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq2" class="collapsed">¿Cuáles son los métodos de pago permitidos para adquirir un paquete? <i class="ion-android-remove"></i></a>
            <div id="faq2" class="collapse" data-parent="#faq-list">
              <p>
            Por el momento solo aceptamos PAYPAL como medio de pago.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq3" class="collapsed">¿Qué requisitos debo cumplir para hacer uso de los paquetes adquiridos?<i class="ion-android-remove"></i></a>
            <div id="faq3" class="collapse" data-parent="#faq-list">
              <p>
              Solo debes estar registrado en el SAT (Secretaria de Administración Tributaria) y expedir facturas.
                </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq4" class="collapsed">¿Tiene fecha de vencimiento mí paquete? <i class="ion-android-remove"></i></a>
            <div id="faq4" class="collapse" data-parent="#faq-list">
              <p>
              No, nuestros timbres estaran disponibles siempre que los necesite.
              </p>
            </div>
          </li>

          <li>
            <a data-toggle="collapse" href="#faq5" class="collapsed">¿En qué momento puedo hacer uso de mi paquete adquirido?<i class="ion-android-remove"></i></a>
            <div id="faq5" class="collapse" data-parent="#faq-list">
              <p>
              En cuanto se verifique su pago.
              </p>
            </div>
          </li>
        </ul>

      </div>
    </section><!-- #faq -->

    <!--==========================
      Our Team Section
    ============================-
    <section id="team" class="section-bg">
      <div class="container">
        <div class="section-header">
          <h3 class="section-title">Nuestro Equipo</h3>
          <span class="section-divider"></span>
          <p class="section-description">Conoce el equipo de OBX</p>
        </div>
        <div class="row wow fadeInUp">
          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team/team-1.jpg" alt=""></div>
              <h4>Willis Garcia</h4>
              <span>-jefe</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team/team-2.jpg" alt=""></div>
              <h4>Fabiola Vasquez</h4>
              <span>Product Manager</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team/team-3.jpg" alt=""></div>
              <h4>Sergio Javier Garcia</h4>
              <span>CTO</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team/team-4.jpg" alt=""></div>
              <h4>Laura Ramirez</h4>
              <span>c</span>
              <div class="social">
                <a href=""><i class="fa fa-twitter"></i></a>
                <a href=""><i class="fa fa-facebook"></i></a>
                <a href=""><i class="fa fa-google-plus"></i></a>
                <a href=""><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section><!-- #team -->

    <!--==========================
      Gallery Section
    ============================-
    <section id="gallery">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title">Galeria</h3>
          <span class="section-divider"></span>
          <p class="section-description"></p>
        </div>

        <div class="row no-gutters">

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="img/gallery/gallery-1.jpg" class="gallery-popup">
                <img src="img/gallery/gallery-1.jpg" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="img/gallery/gallery-2.jpg" class="gallery-popup">
                <img src="img/gallery/gallery-2.jpg" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="img/gallery/gallery-3.jpg" class="gallery-popup">
                <img src="img/gallery/gallery-3.jpg" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="img/gallery/gallery-4.jpg" class="gallery-popup">
                <img src="img/gallery/gallery-4.jpg" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="img/gallery/gallery-5.jpg" class="gallery-popup">
                <img src="img/gallery/gallery-5.jpg" alt="">
              </a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6">
            <div class="gallery-item wow fadeInUp">
              <a href="img/gallery/gallery-6.jpg" class="gallery-popup">
                <img src="img/gallery/gallery-6.jpg" alt="">
              </a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #gallery -->

    <!--==========================
      Contact Section
    ============================-
    <section id="contact">
      <div class="container">
        <div class="row wow fadeInUp">

          <div class="col-lg-4 col-md-4">
            <div class="contact-about">
              <h3>OBX</h3>
              <p>OBX es una empresa comprometida con sus clientes, por favor si tiene alguna duda o sugerencia le invitamos a hacernosla llegar.</p>
              <div class="social-links">
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="info">
              <div>
                <i class="ion-ios-location-outline"></i>
                <p>Privada de Gardenias #100<br>Colonia Reforma, Oaxaca de Juarez</p>
              </div>

              <div>
                <i class="ion-ios-email-outline"></i>
                <p>soporte@obx.com</p>
              </div>

              <div>
                <i class="ion-ios-telephone-outline"></i>
                <p>+1 5589 55488 55s</p>
              </div>

            </div>
          </div>
          <div style="width: 400px; height: 400px;">
	{!! Mapper::render() !!}
</div>


      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>OBX</strong>. All Rights Reserved
          </div>


          <div class="credits">
            <!--
              All the links in the footer should remain intact.
              You can delete the links only if you purchased the pro version.
              Licensing information: https://bootstrapmade.com/license/
              Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Avilon
            -->
           <!-- Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
                  </div>
        </div>
        <div class="col-lg-6">
          <nav class="footer-links text-lg-right text-center pt-2 pt-lg-0">
            <a href="#intro" class="scrollto">Home</a>
            <a href="#about" class="scrollto">Acerca de</a>
            <a href="#">Politica de Privacidad</a>
            <a href="#">Terminos y condiciones</a>
          </nav>
        </div>
      </div>
    </div>
  </footer><!-- #footer 

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script src="js/contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
