<!DOCTYPE html>
<html lang="en">
<head>
  <title>Paciente</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- icono para la pagina-->
<link rel="shortcut icon" href="imagenes/enfermera.png" type="image/png">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Ingrese los datos del Paciente</h2>
  <form class="form-horizontal" action="" method="post">
    <div class="form-group">
      <label class="control-label col-sm-2">Nombre del Paciente:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="10" placeholder="Ingrese nombre completo" name="username" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">NSS:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese el nss del paciente"required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Sexo:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese el sexo del paciente"required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Edad del paciente:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="18"placeholder="Ingrese la edad">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Peso:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30" placeholder="Ingrese el peso del paciente" required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Estatura:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese la estatura del paciente"required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Direccion:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese la direccion del paciente"required>
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2">Padecimiento:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" maxlength="30"placeholder="Ingrese el nombre del padecimiento"required>
      </div>
    </div>
      <div class="form-group">
        <label class="control-label col-sm-2">alergias:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="30"placeholder="Ingrese las alergias del paciente"required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Tipo de Sangre:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="30"placeholder="Ingrese el tipo de sangre del paciente"required>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-sm-2">Tratamientos:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="30"placeholder="Ingreselos tratamientos que lleva el paciente"required>
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-sm-2">Otros:</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" maxlength="30"placeholder="Ingrese "required>
        </div>
      </div>

    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Actualizar</button>
    </div>
  </form>
</div>

</body>
</html>
<style>
    #form-escolaridad{
    }
</style>
