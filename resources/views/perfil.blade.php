<?php
session_start();
echo $_SESSION['tipo'];
if ($_SESSION['estado']!='Autenticado') {
   header("location: /seguridad");
   exit();
}
if ($_SESSION['tipo']!='enfermero') {
 header("location: /");
exit();
}


$id=$_SESSION['id'];

$username = "root";
$password = "";  // en mi caso tengo contraseña pero en casa caso introducidla aquí.
$servername = "localhost";
$database = "enfermeres";
$conexion = mysqli_connect($servername, $username, $password);
$db = mysqli_select_db( $conexion, $database );

$result=mysqli_query($conexion,"select id_enfermero,foto, CedulaProf, Nombre,Apellido_pat,Apellido_mat,edad,escolaridad,Telefono, Direccion, correo,password ,status,contratacion,costo,descripcion FROM enfermero WHERE id_enfermero= '$id'");

while($row = mysqli_fetch_array($result)){
   $id=$row['id_enfermero'];
    $foto=$row['foto'];
   $nombre=$row['Nombre'];
   $ap=$row['Apellido_pat'];
   $am=$row['Apellido_mat'];
   $edad=$row['edad'];
   $escolaridad=$row['escolaridad'];
   $telefono=$row['Telefono'];
   $direccion=$row['Direccion'];
   $costo=$row['costo'];
   $descripcion=$row['descripcion'];


}


?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="csrf-token" content="{{ csrf_token() }}">
          <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <title>Panel Cliente</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css')}}">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="{{ asset('https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700')}}" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contacto</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/panelCliente" class="brand-link">

      <span class="brand-text font-weight-light">Panel de Cliente</span>
    </a>


    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">

        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ¿Que desea hacer?
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
  
              <li class="nav-item">
                <a href="/catalogo" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Catalogo de Enfermeros</p>
                </a>
              </li>

       
                     <li class="nav-item">
                <a href="/pendienteActivacion" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pendientes de Pago</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/pendientePago" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>En curso</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="/Historial" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Historial de Servicios</p>
                </a>
              </li>
              <li class="nav-item">
                <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
              </li>

            </ul>
          </li>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>

            
            <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"></h1>
       
<form class="form-horizontal" method="post"  action="/EnvioActualizacionesEnfermeros"  enctype="multipart/form-data"> 
@csrf

<input type="hidden" class="form-control"   name="ide" readonly="readonly" value="<?php echo $_SESSION['id'];?>">

<label class="control-label col-sm-2">Nombre:</label>
<input type="text" class="form-control"   name="nombre" readonly="readonly" value="<?php echo $nombre;?>">
<label class="control-label col-sm-2">Apellido Paterno:</label>
<input type="text" class="form-control"   name="app" readonly="readonly" value="<?php echo $ap;?>">
<label class="control-label col-sm-2">Apellido Materno:</label>
<input type="text" class="form-control"   name="am" readonly="readonly" value="<?php echo $am;?>">
<label class="control-label col-sm-2">Telefono:</label>
<input type="text" class="form-control"   name="telefono" readonly="readonly" value="<?php echo $telefono;?>">
<label class="control-label col-sm-2">Escolaridad:</label>
<input type="text" class="form-control"   name="escolaridad" readonly="readonly" value="<?php echo $escolaridad;?>">
<label class="control-label col-sm-2">Edad:</label>
<input type="text" class="form-control"   name="edad" readonly="readonly" value="<?php echo $edad;?>">
<label class="control-label col-sm-2">Direccion:</label>
<input type="text" class="form-control"   name="direccion" readonly="readonly" value="<?php echo $direccion;?>">
<label class="control-label col-sm-2">Precio:</label>
<input type="text" class="form-control"   name="costo" readonly="readonly" value="<?php echo $costo;?>">
<label class="control-label col-sm-2">Descripcion:</label>
<input type="text" class="form-control"   name="descripcion" readonly="readonly" value="<?php echo $descripcion;?>">


  <br>
  <br>



<a  href='solicitud/{$id}' class='btn btn-primary'>Contratar Enfermero</a>

  </form>

     
          </div><!-- /.col -->

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>



          </div><!-- /.col -->

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<script type="text/javascript">

  function call()
{
  pagina1();
  pagina2();
}

function pagina1()
{
 document.location.href='/envio';
}

function pagina2()
{
 document.location.href='/pago';
}

</script>

<!-- AdminLTE App -->
<script src="{{ asset('dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
</body>
</html>
