<?php 
session_start();
//echo $_SESSION['tipo'];

if ($_SESSION['estado']!='Autenticado') {
   include("/seguridad");
   exit();
} 

if ($_SESSION['tipo']!='administrador') {
 header("location: /");
 exit();
}

?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include the above in your HEAD tag ---------->

<!--author:starttemplate-->
<!--reference site : starttemplate.com-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="keywords"
          content="unique login form,leamug login form,boostrap login form,responsive login form,free css html login form,download login form">
    <meta name="author" content="leamug">
    <title>Inicia Sesion</title>

    <link href="css/style-admi.css" rel="stylesheet" id="styleAd">
    <!-- Bootstrap core Library -->
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!-- Google font -->
    <link href="https://fonts.googleapis.com/css?family=Dancing+Script" rel="stylesheet">
    <!-- Font Awesome-->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-4 text-center">
            <h1 class='text-white'>Unique Login Form</h1>
            <!-- <a class="btn btn-danger btn-md" href="https://starttemplates.com" target="_blank">Ingresa<i class="fa fa-download pl-2"></i></a> -->
            <div class="form-login"></br>
                <h4>Ingresa a tu cuenta</h4>
                </br>
                <input type="text" id="userName" class="form-control input-sm chat-input" placeholder="email"/>
                </br></br>
                <input type="text" id="userPassword" class="form-control input-sm chat-input" placeholder="contraseña"/>
                </br></br>
                <div class="wrapper">
                        <span class="group-btn">
                            <a href="#" class="btn btn-primary btn-md">ingresar <i class="fa fa-sign-in"></i></a>
                        </span>
                </div>
            </div>
        </div>
    </div>
    </br></br></br>
    <!--footer-->
    <div class="footer text-white text-center">
        <p>© 2019 Unique Login Form. All rights reserved | Design by <a href="/" rel="dofollow">HomeCare</a></p>
    </div>
    <!--//footer-->
</div>
</body>
</html>
