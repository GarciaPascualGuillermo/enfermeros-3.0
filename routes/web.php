<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Front
// Back
Route::resource('registro', 'Controller');
Route::get('/inicia','Controller@vistalogin');
Route::post('/submit', 'Controller@acceso_login');
Route::get('registro','Controller@vistaregistro');

Route::post('manejador','PrincipalController@manejador');
Route::get('homeAdmin','PrincipalController@admin');
Route::post('/consulta','PanelController@consulta');
Route::get('catalogo/perfil/{id}','catalogoController@getPerfil');
Route::get('catalogo/perfil/solicitud/{id}','PacienteController@registroPaciente');
Route::get('/Historial','PanelController@historial');
Route::get('/pendientePago','PanelController@pendiente');
Route::get('/pendienteActivacion','PanelController@solicitud');
Route::get('acceso','Controller@acceso_directo');
Route::get('editar_perfil','Controller@vista_editar_perfil');
Route::get('contacto','Controller@vista_contacto');
Route::get('carrito','Controller@vista_carrito');
Route::get('sesion','Controller@vista_sesion');
Route::get('principalEnfermero','PrincipalController@enfermeroP');
Route::post('envio', 'Controller@insertarUsuario');

Auth::routes();
Route::get('/home', 'DashboardController@index')->name('home');

//pagos
Route::get('cancelar/{id}','PanelController@cancelar');


Route::get('/', 'PrincipalController@index');
Route::get('/admin', 'PrincipalController@loginAdmin');   // VISTA DEL LOGUIN DEL ADMINISTRADOR

Route::get('/login', 'PrincipalController@login2');//mod

Route::resource('paciente','PacienteController');
Route::get('/loginEnfermeras', 'PrincipalController@loginEnfermeras');
Route::get('/registropaciente','PanelController@store');

Route::get('autenticacion/{provider}', 'Auth\SocialAuthController@redirectToProvider')->name('social.auth');
Route::get('autenticacion/{provider}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');



//Rutas Enfermer@s
Route::get('/datosEnfermera', 'DatosEnfermerasController@enfermeras');
Route::post('/datosEnfermera', 'DatosEnfermerasController@enfermeras');
Route::post('/registroEnfermero','DatosEnfermerasController@registrar');
Route::get('/registroEnfermero','DatosEnfermerasController@registrar');
Route::post('/UpdateEnfermero','DatosEnfermerasController@actualizar');
Route::get('/UpdateEnfermero','DatosEnfermerasController@actualizar');
Route::get('/pruba','DatosEnfermerasController@registroEnfermero');
Route::get('/catalogo','catalogoController@catalogo');
Route::get('/verPacientes','PanelController@verPaciente');
Route::get('/aceptar/{id}','EnfeController@aceptar');
Route::get('/rechazar/{id}','EnfeController@rechazar');


//rutas Panel de Enfermeros
Route::get('/panelEnfe','EnfeController@index');
Route::get('/perfilEnfe','EnfeController@Perfil');
Route::get('/PacienteEnfe','EnfeController@VerPacientes');
Route::get('/ContactoEnfe','EnfeController@Contacto');
Route::get('/solicitudes', 'EnfeController@verSolicitud');
Route::get('/solicitudesPagadas', 'EnfeController@verPagadas');
Route::get('/solicitudesProceso', 'EnfeController@verProceso');
Route::get('/HistorialEnfe','EnfeController@Historial');
Route::get('/logout2','PrincipalController@cerrarSesion');
Route::get('/panelAdmin','PanelAdminController@index');
Route::get('/AdminEnfermero','PanelAdminController@AdminEnfermero');
Route::post('/AdminEnfermero','PanelAdminController@AdminEnfermero');
Route::get('/AdminVerEnfermero','PanelAdminController@AdminVerEnfermero');
Route::post('ajaxxx','PrincipalController@ajaxito');
Route::get('ajaxxx','PrincipalController@ajaxito');
Route::post('ajax2','PrincipalController@ajaxito2');
Route::get('ajax2','PrincipalController@ajaxito2');
Route::post('/EnvioActualizacionesEnfermeros','EnfeController@setUpdate');


//Rutas Pacientes

Route::get('/datosPaciente', 'PacienteController@paciente');
Route::post('/registrarPaciente','PacienteController@registrarPaciente');
//Route::get('/registroPaciente','PacienteController@registroPaciente');
//Route::post('/registroPaciente','PacienteController@registroPaciente');
Route::post('ajaxnsS','PrincipalController@ajaxB1');
Route::get('ajaxnsS','PrincipalController@ajaxB1');
Route::post('/rellenar','PrincipalController@pacientito');
Route::get('/rellenar','PrincipalController@pacientito');

//.........................
Route::get('/panelCliente', 'PanelController@index');
//Route::get('/envio', 'PanelController@mensajeTelegram');
//Route::get('pago','PrincipalController@pagoStripe');
//Route::post('pago','PanelController@pagoStripe');

Route::post('/chekin','PanelController@chekin');
Route::get('/envio','PanelController@mensajeTelegram');
Route::get('/pago/{costo}/{id_servicio}','PanelController@pagoStripe');


//Route::post('/chekin','PanelController@chekin');


//Rutas Clientes
Route::get('/datosCliente', 'PanelController@datosCliente');
Route::post('/registroCliente','datosClientesController@registrarCliente');
Route::get('/prueba','datosClientesController@registroCliente');
Route::get('/ubicacion','PanelController@ubicacion');
Route::post('/ubicacion','PanelController@ubicacion');
Route::post('validate','PrincipalController@validate');
//aqui
Route::post('/Master', 'PrincipalController@master');
Route::get('/Master', 'PrincipalController@master');

Route::post('ajaxx','PrincipalController@ajax');
Route::get('ajaxx','PrincipalController@ajax');
//ajaxCedula
Route::post('ajaxxcedula','PrincipalController@ajaxCedula');
Route::get('ajaxxcedula','PrincipalController@ajaxCedula');

Route::post('/mas', function () {
   return view('master');
});
Route::get('/mas', function () {
   return view('master');
});
//aqui

Route::get('/seguridad','PrincipalController@seguridad');
Route::get('/cerrar','PrincipalController@cerrarSesio');

Route::get('/Servicios', 'PrincipalController@servicio');
Route::get('/Nosotros', 'PrincipalController@nstrs');


//Rutas Administrador

Route::get('/inicioAdministrador',
          ['as'=>'inicioAdministrador',
          'uses'=>'AdministradorController@administrador']);
Route::get('enfermeras/{slug}',
          [ 'as'=>'enfermeras_lista',
          'uses'=>'AdministradorController@show' ]);

Route::get('/loginAdmin','PrincipalController@loginAdmin');
//Route::post('/registropaciente','PacienteController@registrarPaciente');
//Route::get('/prubaPaciente','PacienteController@registroPaciente');

//Route::get('/datosClientes', 'datosClientesController@clientes');


    // ************PACIENTE**********

    //Route::get('/registropaciente','PacienteController@store');

     //Route::get();
    //socialite de google
Route::get('/social/redirect/{provider}', 'Auth\SocialController@getSocialRedirect')->name('redirectSocialLite');
Route::get('/social/handle/{provider}', 'Auth\SocialController@getSocialHandle')->name('handleSocialLite');

 //Route::get('/registropaciente','PacienteController@store');


Auth::routes();


Auth::routes();


Auth::routes();
