<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix' => 'v1', 'as' => 'api.', 'namespace' => 'Api\V1\Admin', 'middleware' => ['auth:api']], function () {
    // Permissions
    Route::apiResource('permissions', 'PermissionsApiController');

    // Roles
    Route::apiResource('roles', 'RolesApiController');

    // Users
    Route::apiResource('users', 'UsersApiController');

    // Product Categories
    Route::post('product-categories/media', 'ProductCategoryApiController@storeMedia')->name('product-categories.storeMedia');
    Route::apiResource('product-categories', 'ProductCategoryApiController');

    // Product Tags
    Route::apiResource('product-tags', 'ProductTagApiController');

    // Products
    Route::post('products/media', 'ProductApiController@storeMedia')->name('products.storeMedia');
    Route::apiResource('products', 'ProductApiController');
});


Route::group(['prefix'=>'auth'],function (){
    Route::get('black','Auth\LoginController@blacklist');
    Route::post('insertar', 'Auth\LoginController@insertar_black');
    Route::put('actualizar', 'Auth\LoginController@actualizar_black');
    Route::put('eliminar', 'Auth\LoginController@eliminar_black');

    Route::get('busquedaNombre','Auth\LoginController@blacklist_nombre');
    Route::post('insertar1', 'Auth\LoginController@insertar');

    Route::put('actualizar_bd', 'Auth\LoginController@actualizar');
    Route::put('eliminar_bd', 'Auth\LoginController@eliminar');
    
});