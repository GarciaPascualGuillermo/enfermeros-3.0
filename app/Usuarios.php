<?php

namespace HomeCare;

use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    //
    protected $table = 'persona';
 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = ['contrasenia', 'razon_social', 'rfc', 'nombre', 'curp', 'f_nac', 'genero','ap_paterno', 'activo', 'ap_materno', 'nombre_repres_legal', 'tipo_sociedad', 'folio_ife', 'edo_civil', 'tipo', 'num_tel', 'sesion'];
    
}

protected $fillable = [
        'name', 'email', 'password','provider','provider_id'
    ];
