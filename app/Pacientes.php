<?php

namespace HomeCare;

use Illuminate\Database\Eloquent\Model;

class Pacientes extends Model
{
    //
    protected $table = 'paciente';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps = false;
    protected $fillable = ['nombre', 'edad', 'padecimiento', 'peso', 'estatura', 'sexo', 'alergias','tiposangre', 'direccion', 'tratamientos', 'nss', 'otros'];

}
