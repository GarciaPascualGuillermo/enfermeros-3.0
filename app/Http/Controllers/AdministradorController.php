<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use HomeCare\Pacientes; // Instanciamos el modelo Usuarios
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\ItemCreateRequest;
use HomeCare\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Input;
use Storage;
use HomeCare\Http\Models;
use HomeCare\Http\Models\EnfermerasModel;

class AdministradorController extends Controller
{
  public function administrador()
  {
      $enfermeras = EnfermerasModel::all();
      return view('homeAdministrador',compact('enfermeras'));
  }

  public function show($slug)
  {
    $enfermera = EnfermerasModel::where('slug','enfermeras_lista')->get()->first();
    return view('store.partials.show',compact('enfermera'));
  }

}
