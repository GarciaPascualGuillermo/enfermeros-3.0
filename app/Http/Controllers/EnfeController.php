<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\ItemCreateRequest;
use HomeCare\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use HomeCare\Mail\Confirmacion;
use HomeCare\Mail\Cancelacion;

use DB;
use Mail;
use Input;
use Storage;

class EnfeController extends Controller
{
   public function index()
    {
     /*session_start();
               // echo $_SESSION['tipo'];
        if ($_SESSION['estado']!='Autenticado') {
                 return view('loginEnfermeras');
        }elseif ($_SESSION['tipo']!='enfermero') {
             return view('loginEnfermeras');

         }else{*/
        return view('enfermero.index');
        // return view('enfermero.index');
        }


    public function verSolicitud(){
      return view('enfermero.solicitudes');
    }
    public function verPagadas(){
      return view('enfermero.solPagadas');
    }

    public function verProceso(){
      return view('enfermero.solProceso');
    }


    public function VerPacientes()
    {
        return view('enfermero.pacientes');
    }
    public function Perfil()
    {

       
        
        return view('enfermero.perfil'); 

    }

    public function setUpdate(Request $request){
    
    $id_enfermero = $request ->input('ide');
    $nombre = $request ->input('nombre');
    $app = $request ->input('app');
    $am = $request ->input('am');
    $telefono = $request ->input('telefono');
    $escolaridad = $request ->input('escolaridad');
    $edad = $request ->input('edad');
    $direccion = $request ->input('direccion');
    $costo = $request ->input('costo');
    $descripcion = $request ->input('descripcion');
    return view('enfermero.UpdatePerfilEnfermero',compact('id_enfermero')); 
    }

    public function Contacto()
    {
        return view('enfermero.contacto');
    }
    public function Historial()
    {
        return view('enfermero.historial');
    }

    public function aceptar($id){
    // $id =  $_GET['id'];
      DB::update('UPDATE servicio
    SET status="Aceptado" WHERE id_servicio="'.$id.'"');
    $results= DB::select('SELECT email FROM servicio INNER JOIN users ON servicio.id_user=users.id WHERE id_servicio="'.$id.'"');
    $correo=$results[0]->email;
      //Aqui van los mensjaes de correo electronico
      Mail::to($correo)->send(new Confirmacion());

       return redirect()->to('/panelEnfe');

    }

     public function rechazar($id){
    // $id =  $_GET['id'];
      DB::update('UPDATE servicio
    SET status="Rechazado" WHERE id_servicio="'.$id.'"');
        $results= DB::select('SELECT email FROM servicio INNER JOIN users ON servicio.id_user=users.id WHERE id_servicio="'.$id.'"');
        $correo=$results[0]->email;
        Mail::to($correo)->send(new Cancelacion());

       //Aqui van los mensjaes de correo electronico
      return redirect()->to('/panelEnfe');
    }

}
