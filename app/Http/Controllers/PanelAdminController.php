<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request; // Instanciamos el modelo Usuarios
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\ItemCreateRequest;
use HomeCare\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Input;
use Storage;
use HomeCare\Http\Models;
use HomeCare\Http\Models\PacienteModel;
use HomeCare\Mail\EmergencyCallReceived;
use Mail;

class PanelAdminController extends Controller
{
  public function index()
  {
      return view('PanelAdmin.index');
  }
  public function AdminEnfermero()
  {
      
      return view('PanelAdmin.AdminEnfermero');
     
  }
  public function AdminVerEnfermero()
  {
      return view('PanelAdmin.AdminVerEnfermero');
  }
  

}
