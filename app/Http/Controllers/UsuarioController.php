<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use HomeCare\Usuarios; // Instanciamos el modelo Usuarios 
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\ItemCreateRequest;
use HomeCare\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Input;
use Storage;

class UsuarioController extends Controller
{
    public function index()
    {
    	$usuarios = usuarios::all();
        return view('admin.usuarios.index', compact('usuarios')); 
    }
    public function create()
    {
        $usuarios = usuarios::all();
        return view('admin.usuarios.create', compact('usuarios'));
    }
    public function edit($id)
    {
        $usuarios = usuarios::find($id);
        return view('admin/usuarios.edit',['usuarios'=>$usuarios]);
    }

    public function new()
    {
        $usuarios = usuarios::all();
        return view('admin.login.registro', compact('usuarios'));
    }

    public function update(ItemUpdateRequest $request, $id)
{        
    $usuarios = Usuarios::find($id);
 
    $usuarios->nombre= $request->nombre;
    $usuarios->ap_paterno= $request->ap_paterno;
    $usuarios->ap_materno= $request->ap_materno;
    $usuarios->tipo= $request->tipo;
    $usuarios->contrasenia= $request->contrasenia;
    $usuarios->razon_social= $request->razon_social;
    $usuarios->rfc= $request->rfc;
    $usuarios->curp= $request->curp;
    $usuarios->f_nac= $request->f_nac;
    $usuarios->genero= $request->genero;
    $usuarios->activo= $request->activo;
    $usuarios->nombre_repres_legal= $request->nombre_repres_legal;
    $usuarios->tipo_sociedad= $request->tipo_sociedad;
    $usuarios->folio_ife= $request->folio_ife;
    $usuarios->edo_civil= $request->edo_civil;
    $usuarios->Num_tel= $request->Num_tel;
    $usuarios->sesion= $request->sesion;

 
    $usuarios->save();
 
    Session::flash('message', 'Editado Satisfactoriamente !');
    return Redirect::to('admin/usuarios');
}

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ItemCreateRequest $request)
    {
        $usuarios = new usuarios;
 
        $usuarios->nombre= $request->nombre;
        $usuarios->ap_paterno= $request->ap_paterno;
        $usuarios->ap_materno= $request->ap_materno;
        $usuarios->tipo= $request->tipo;
        $usuarios->contrasenia= $request->contrasenia;
        $usuarios->razon_social= $request->razon_social;
        $usuarios->rfc= $request->rfc;
        $usuarios->curp= $request->curp;
        $usuarios->f_nac= $request->f_nac;
        $usuarios->genero= $request->genero;
        $usuarios->activo= $request->activo;
        $usuarios->nombre_repres_legal= $request->nombre_repres_legal;
        $usuarios->tipo_sociedad= $request->tipo_sociedad;
        $usuarios->folio_ife= $request->folio_ife;
        $usuarios->edo_civil= $request->edo_civil;
        $usuarios->Num_tel= $request->Num_tel;
        $usuarios->sesion= $request->sesion;

        $usuarios->save();
 
        return redirect('admin/usuarios')->with('message','Guardado Satisfactoriamente !');
    }

    


    public function destroy($id){
        $imagen = usuarios::find($id);
 
        usuarios::destroy($id);        
 
        Session::flash('message', 'Eliminado Satisfactoriamente !');
        return Redirect::to('admin/usuarios');
    }
}
