<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\ItemCreateRequest;
use HomeCare\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use HomeCare\Mail\Pago;
use DB;
use Mail;
use Input;
use Storage;

class PanelController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('PanelCliente.index');
    }
    public function datosCliente()
    {
        return view('PanelCliente.datosCliente'); 
    }
 

 public function mensajeTelegram(){
   
    return view('PanelCliente.messenger');
    }

public function verPaciente(){

    return view('PanelCliente.verPacientes');
}
public function registroPaciente($id){
    return view('paciente/paciente', compact('id'));//vistas
  }
public function registrarPaciente(Request $request)
  {
      $nombre = $request ->input('nombre');
      $paterno=$request ->input('app');
      $materno =$request ->input('apm');
      $edad = $request ->input('edad');
      $padecimiento = $request ->input('padecimiento');
      $peso = $request ->input('peso');
      $estatura = $request ->input('estatura');
      $sexo =$request ->input('sexo');
      $alergias =$request ->input('alergia');
      $tipo =$request ->input('tipo');
      $direccion = $request ->input('direccion');
      $tratamiento = $request ->input('tratamiento');
      $nss =$request ->input('nss');
      $otros = $request ->input('otro');

      PacienteModel::create(['nombre'=>$nombre,'apellido_pat' =>$paterno,
      'apellido_mat'=>$materno,'edad'=>$edad,'padecimiento'=>$padecimiento,'peso'=>$peso,'estatura'=>$estatura,'sexo'=>$sexo,
      'alergias'=>$alergias,'tiposangre'=>$tipo,'direccion'=>$direccion,'tratamientos'=>$tratamiento,'nss'=>$nss,'otros'=>$otros,'id_cliente'=>'1']);
      return 'Registrado';

  }

  
public function chekin(Request $request){
$id_servicio =$request->get('id_servicio');
$costo=$request->get('costo');
DB::update('UPDATE servicio
    SET status="Pagado" WHERE id_servicio="'.$id_servicio.'"');

$results= DB::select('SELECT correo FROM servicio INNER JOIN enfermero ON
 servicio.id_enfermero=enfermero.id_enfermero WHERE id_servicio="'.$id_servicio.'"');
$correo=$results[0]->correo;
  //Aqui van los mensjaes de correo electronico
  Mail::to($correo)->send(new Pago());


return view('PanelCliente.charge',compact('costo'));
}

public function cancelar($id){
DB::update('UPDATE servicio
 SET status="Cancelado" where id_servicio="'.$id.'"');

  return redirect()->to('/panelCliente');//vistas
  }

public function historial(){
   
    return view('PanelCliente.historial');
    }

    public function solicitud(){
   
        return view('PanelCliente.pendienteConfirmacion');
        }

        public function pendiente(){
   
            return view('PanelCliente.pendientePago');
            }

public function ubicacion(Request $request){

    $method = $request->method();


if ($request->isMethod('post')) {

    $cedula=$request->input('cedula');
    return view('PanelCliente.ubicacion',compact('cedula'));

}

if ($request->isMethod('get')) {
          // $cedula=$request->input('cedula');*/
    return view('PanelCliente.ubicacion');

}



}

public function pagoStripe($pago,$id_servicio){
    

    return view('PanelCliente.stripe',compact('pago','id_servicio'));
}

 public function consulta(){
        return  view('consulta');
    }

}
