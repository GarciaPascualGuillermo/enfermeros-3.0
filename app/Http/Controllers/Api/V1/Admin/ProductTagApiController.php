<?php

namespace HomeCare\Http\Controllers\Api\V1\Admin;

use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\StoreProductTagRequest;
use HomeCare\Http\Requests\UpdateProductTagRequest;
use HomeCare\Http\Resources\Admin\ProductTagResource;
use HomeCare\ProductTag;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class ProductTagApiController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('product_tag_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductTagResource(ProductTag::all());
    }

    public function store(StoreProductTagRequest $request)
    {
        $productTag = ProductTag::create($request->all());

        return (new ProductTagResource($productTag))
            ->response()
            ->setStatusCode(Response::HTTP_CREATED);
    }

    public function show(ProductTag $productTag)
    {
        abort_if(Gate::denies('product_tag_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return new ProductTagResource($productTag);
    }

    public function update(UpdateProductTagRequest $request, ProductTag $productTag)
    {
        $productTag->update($request->all());

        return (new ProductTagResource($productTag))
            ->response()
            ->setStatusCode(Response::HTTP_ACCEPTED);
    }

    public function destroy(ProductTag $productTag)
    {
        abort_if(Gate::denies('product_tag_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $productTag->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
