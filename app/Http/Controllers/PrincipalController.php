<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare;
use HomeCare\Http\Models\EnfermerasModel;
use DB;
use Mail;
use HomeCare\Mail\EmergencyCallReceived2;
//use HomeCare\Http\Models\Enfermeros;
/**
      * Display a listing of the resource.
      *
      * @return \Illuminate\Http\Response
      */

class PrincipalController extends Controller
{
    public function index()
    {
        return view('admin.principal.index3');
    }

    public function login(){
        return view('loginR');
    }

    public function loginEnfermeras(){
        return view('auth/login');
    }

     public function login2(){
        return view('loginR');
    }

    public function manejador(){
        return  view('manejadorSesion');
    }

     public function admin(){
        return  view('homeAdministrador');
    }
    public function enfermeroP(){
        return  view('homeEnfermero');
    }

    public function cerrarSesion(){
        return  view('cerrarSesion');
    }

    public function loginAdmin(){
        return  view('PanelAdmin.loginAdmi');
    }


    public function servicio(){
      return view('admin.principal.Servicios');
    }

    public function nstrs(){
      return view('admin.principal.Nosotros');
    }

    /*
    public function validate(){
     //$results =  DB::select('SELECT * FROM enfermero WHERE correo = ?',['correo'] ;
        echo "im in AjaxController index";//simplemente haremos que devuelva esto

    }






*/
  public function ajax(){
    $correo=$_POST["correo"];
     $users=DB::select('SELECT * FROM enfermero WHERE correo= "'.$correo.'"');
     //print_r ($users);
        if(count($users)>0){
          echo '<div class="alert alert-danger"><strong>Oh no!</strong> Correo no disponible.</div>  <script>
document.getElementById("boton").disabled=true;
</script>';
    } else {
        echo '<div class="alert alert-success"><strong>Enhorabuena!</strong> Correo disponible.</div> <script>
document.getElementById("boton").disabled=false;
</script>';

        }
     }


     public function ajaxCedula(){
    $cedula=$_POST["CedulaProf"];
     $users=DB::select('SELECT * FROM enfermero WHERE CedulaProf= "'.$cedula.'"');
     //print_r ($users);
        if(count($users)>0){
          echo '<div class="alert alert-danger"><strong>Oh no!</strong> Esta cedula ya ha sido utilizada.</div>  <script>
document.getElementById("boton").disabled=true;
</script>';
    } else {
        echo '<div class="alert alert-success"><strong>Enhorabuena!</strong> Cedula Aceptada.</div> <script>
document.getElementById("boton").disabled=false;
</script>';

        }
     }

     public function ajaxB1(){
    $nss=$_POST["nss"];
     $users=DB::select('SELECT * FROM paciente WHERE nss= "'.$nss.'"');
     //print_r ($users);
        if(count($users)>0){
          echo '<div class="alert alert-danger"><strong>Oh no!</strong> Esta cedula ya ha sido utilizada.</div>  <script>
          document.getElementById("boton").disabled=true;
          </script>';
    } else {
              echo '<div class="alert alert-success"><strong>Enhorabuena!</strong> Cedula Aceptada.</div> <script>
      document.getElementById("boton").disabled=false;
      </script>';

        }
     }

     public function ajaxito(){
      $id = $_POST["id_enfermero"];
      //echo $id;
          DB::update('UPDATE enfermero
    SET contratacion="contratado" WHERE id_enfermero="'.$id.'"');
    $results = DB::select('SELECT correo from enfermero WHERE id_enfermero="'.$id.'"');
    $correo = $results[0]->correo;
    //print_r($correo);
    Mail::to($correo)->send(new EmergencyCallReceived2());

        }


     public function master()
     {
         return view('master');
     }

     public function pacientito(){
      return view('vistaPaciente');
     }

     public function seguridad(){
        return view('seguridad');
     }

     public function cerrarSesio(){
        return view('cerrarSesion');
     }
}
