<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use HomeCare\Pacientes; // Instanciamos el modelo Usuarios
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\ItemCreateRequest;
use HomeCare\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Mail;
use HomeCare\Mail\notificacionEnfe;
use Input;
use Storage;
use HomeCare\Http\Models;
use HomeCare\Http\Models\PacienteModel;
use Illuminate\Support\Facades\Auth;
class PacienteController extends Controller
{


  public function paciente()
  {
      return view('paciente/paciente');
  }
  public function registroPaciente($id){
    return view('paciente/paciente', compact('id'));//vistas
  }

  public function registrarPaciente(Request $request)
  {
      $fechain = $request->input('fechaInicio');
      $fechafin = $request ->input('fechaFin');
      $nombre = $request ->input('nombre');
      $edad =$request->input('edad');
      $sexo =$request ->input('sexo');
      $peso = $request ->input('peso');
      $estatura = $request ->input('estatura');
      $tipo = $request->input('tipo');
      $alergias =$request ->input('alergia');
      $descrip =$request ->input('message');
      $direccion = $request ->input('direccion');
      $idUser = $request ->input('id_cliente');
      $idEnfermero=$request ->input('id_enfermero');
      $costo=$request->input('costo');
      PacienteModel::create(['id_user' => $idUser , 'id_enfermero'=> $idEnfermero,'fecha_inicio'=>$fechain,'fecha_final'=>$fechafin,'nombre_paciente'=>$nombre, 'edad'=>$edad,'sexo'=>$sexo,'peso'=>$peso,'estatura'=>$estatura,
      'tipo_sanguineo'=>$tipo,'alergias'=>$alergias,'descripcion'=>$descrip,'direccion'=>$direccion,'costo'=>$costo,'status'=>'Solicitud']);
      $results= DB::select('SELECT correo FROM enfermero WHERE enfermero.id_enfermero="'.$idEnfermero.'"');
      $correo=$results[0]->correo;
      Mail::to($correo)->send(new notificacionEnfe());
      return view('PanelCliente/index');


  }
}
