<?php

namespace HomeCare\Http\Controllers\Admin;

class HomeController
{
    public function index()
    {
        return view('home');
    }
}
