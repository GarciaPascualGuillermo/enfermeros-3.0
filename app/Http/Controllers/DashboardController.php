<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use HomeCare\Pasajeros;
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use HomeCare\Http\Requests\PasajerosCreateRequest;
use HomeCare\Http\Requests\PasajerosUpdateRequest;
use Illuminate\Support\Facades\Validator;
use DB;
use Input;
use Storage;
use Auth;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

 
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        return view('/panelCliente'); 
    }

}
