<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use HomeCare\Http\Controllers\Controller;

class AjaxController extends Controller
{
    public function ajax()
     {
         echo "im in AjaxController index";//simplemente haremos que devuelva esto
     }

     public function master()
     {
         return view('master');
     }
}
