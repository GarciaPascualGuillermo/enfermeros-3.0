<?php

namespace HomeCare\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use HomeCare\Http\Requests;
use HomeCare\Http\Controllers\Controller;
use Mapper;
use HomeCare\Http\Models;
use HomeCare\Mail\EmergencyCallReceived;
use HomeCare\Http\Models\EnfermerasModel;
use Storage;
use Mail;
use DB;

class DatosEnfermerasController extends Controller
{

    public function enfermeras()
    {

        return view('datosEnfermeras');
    }
    public function registroEnfermero(){
      return view('datosEnfermeras');//vistas
    }
    public function registrar(Request $request)
    {

        
        $cedulaProf = $request ->input('cedula');
        $nombre = $request ->input('nombre');
        $paterno=$request ->input('paterno');
        $materno =$request ->input('materno');
        $sexo =$request ->input('sexo');
        $edad = $request ->input('edad');
        $especialidad = $request ->input('escolaridad');
        $telefono = $request ->input('telefono');
        $direccion = $request ->input('direccion');
        $correo = $request ->input('correo');
        $password = $request ->input('pass');
        $costo = $request ->input('costo');
        $descripcion = $request ->input('descripcion');

        $carpeta='images/';
        
        $archivo=$_FILES['imagen']["name"];
        $ruta=$carpeta.$archivo;
        if(!move_uploaded_file($_FILES['imagen']['tmp_name'], $carpeta.$archivo)){

            echo "error al subir imagen";
        }
        


        EnfermerasModel::create(['cedulaProf'=>$cedulaProf,'Nombre'=>$nombre,'Apellido_pat' =>$paterno,
        'Apellido_mat'=>$materno,'Sexo'=>$sexo,'Edad'=>$edad,'escolaridad'=>$especialidad,'Telefono'=>$telefono,'Direccion'=>$direccion,'correo'=>$correo,'password'=>$password,'status'=>'1','contratacion'=>'No contratado','costo'=>$costo,'descripcion'=>$descripcion,'foto'=>$ruta]);

   

        Mail::to($correo)->send(new EmergencyCallReceived());
        echo '<script type="text/javascript">
        alert("Usuario registrado, en breve recibira un correo");
        window.location.href="index.php";
        </script>';



        /*'correo'=>$correo,'Documentos'=>$doc ,'password'=>$password,'id_status'=>'1','contratacion'=>'No contratado']);
        return redirect()->to('/loginEnfermeras');*/


    }


    public function actualizar(Request $request)
    {

 
        $id_enfermero = $request ->input('id_');
        echo $id_enfermero;

        $nombre = $request ->input('nombre');
        $paterno=$request ->input('paterno');
        $materno =$request ->input('materno');
        $sexo =$request ->input('sexo');
        $edad = $request ->input('edad');
        $especialidad = $request ->input('escolaridad');
        $telefono = $request ->input('telefono');
        $direccion = $request ->input('direccion');
        $descripcion = $request ->input('descripcion');
        $costo = $request ->input('costo');



          $carpeta='images/';
        
        $archivo=$_FILES['imagen']["name"];
        $ruta=$carpeta.$archivo;

        if(!move_uploaded_file($_FILES['imagen']['tmp_name'], $carpeta.$archivo)){

            echo "error al subir imagen";
        }
       
       
    
      //echo $id;
          DB::update('UPDATE enfermero
    SET Nombre="'.$nombre.'", Apellido_pat="'.$paterno.'", Apellido_mat="'.$materno.'",Sexo="'.$sexo.'",Edad="'.$edad.'" , escolaridad="'.$especialidad.'",Telefono="'.$telefono.'" , Direccion="'.$direccion.'",costo="'.$costo.'",descripcion="'.$descripcion.'",foto="'.$ruta.'" WHERE id_enfermero="'.$id_enfermero.'"');
      
    return redirect()->to('/panelEnfe');  

    }
}
