<?php
namespace HomeCare\Http\Models;

use Illuminate\Database\Eloquent\Model;



class ClientesModel extends Model{
    //nombre de la tabla
    protected $table = 'users';

    //llave primaria
    protected $primarykey = 'id';
    
    public $timestamps = false;

    protected $fillable = [ 'name', 'apellido_pat', 'apellido_mat', 'sexo', 'telefono','email'];

}