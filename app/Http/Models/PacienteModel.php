<?php
namespace HomeCare\Http\Models;

use Illuminate\Database\Eloquent\Model;



class PacienteModel extends Model{
    //nombre de la tabla
    protected $table = 'servicio';

    //llave primaria
    protected $primarykey = 'id_servicio';
    public $timestamps = false;

    //aqui los elementos a mostrarse en la tabla
    protected $fillable = ['id_user','id_enfermero','fecha_inicio', 'fecha_final', 'nombre_paciente', 'edad', 'sexo', 'peso', 'estatura','tipo_sanguineo', 'alergias', 'descripcion', 'direccion','costo','status'];

}
