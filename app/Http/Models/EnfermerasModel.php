<?php
namespace HomeCare\Http\Models;

use Illuminate\Database\Eloquent\Model;



class EnfermerasModel extends Model{
    //nombre de la tabla
    protected $table = 'enfermero';

    protected $primaryker = 'id_enfermero';
    public $timestamps = false;

    protected $fillable = ['id_enfermero','foto','cedulaProf', 'Nombre', 'Apellido_pat', 'Apellido_mat', 'Sexo', 'Edad','escolaridad','Telefono', 'Direccion','correo','password','status','contratacion','costo','descripcion'];

}
