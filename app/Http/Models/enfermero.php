<?php

namespace HomeCare;

use Illuminate\Database\Eloquent\Model;

class enfermero extends Model
{
  protected $table = 'enfermero';

  protected $primaryker = 'id_enfermero';
  public $timestamps = false;

  protected $fillable = ['cedulaProf', 'Nombre', 'apellido_pat', 'apellido_mat', 'Sexo', 'Edad', 'Especialidad','Telefono', 'Direccion',
                        'correo','password','Documentos','id_status','contratacion'];

}
