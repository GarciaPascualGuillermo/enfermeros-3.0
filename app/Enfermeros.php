<?php

namespace HomeCare;

use Illuminate\Database\Eloquent\Model;

class Enfermeros extends Model
{
    //
    protected $table = 'enfermero';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryker = 'id_enfermero';
    public $timestamps = false;

    protected $fillable = ['id_enfermero', 'cedulaProf', 'Nombre', 'apellido_pat', 'apellido_mat', 'Sexo', 'Edad', 'Especialidad','Telefono', 'Direccion'];
   

    

}
