<?php

namespace HomeCare;

use Illuminate\Database\Eloquent\Model;

class Clientes extends Model
{
    //
    protected $table = 'cliente';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $primarykey = 'id_cliente';
     public $timestamps = false;

    protected $fillable = ['id_cliente','nombre', 'apellido_pat', 'apellido_mat', 'sexo', 'correo', 'telefono'];

}
